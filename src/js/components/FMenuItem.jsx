import React from 'react';
import SelectEntityActionCreator from '../actions/SelectEntityActionCreator';

export default React.createClass({
  getInitialState() {
    return {};
  },

  componentDidMount() {
  },
  
  _onClick() {
    // direct create the action
    SelectEntityActionCreator.selectItem(this.props.children);
    
    // call parent
    {this.props.handleClick()}
  },

  render() {
    return (
     <li role="menuitem" onClick={this._onClick}>
        <a href={this.props.href}> {this.props.children} </a>
     </li>
    );
  }
});
