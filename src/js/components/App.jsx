import React, {PropTypes} from 'react';
import Button from 'react-bootstrap/lib/Button';
import Jumbotron from 'react-bootstrap/lib/Jumbotron';
import Grid from 'react-bootstrap/lib/Grid';
import Col from 'react-bootstrap/lib/Col';
import Row from 'react-bootstrap/lib/Row';


import FDropdown from './FDropdown.jsx';
import FAlertSelection from './FAlertSelection.jsx';

export default React.createClass({
  propTypes: {
   
  },

  getDefaultProps() {
    return {
      tasks: []
    }
  },

  getInitialState() {
    return {data:[{text1:'Editbox'},{text1:'Dropdown'}]};
  },
  render() {
    let {onAddTask, onClear, tasks} = this.props;
    return (
      <div className="container">
        <Jumbotron>
          <h1>Learning React and Flux</h1>
          <p>
            <strong>What you don't know will always hurt you. </strong>
          </p>
        </Jumbotron>

        <Grid>
          <Row className="show-grid">
            <Col xs={6} md={4}>
              <FDropdown data={this.state.data} ></FDropdown> 
            </Col>
            <Col xs={6} md={4}>
              <FAlertSelection  ></FAlertSelection> 
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
});
