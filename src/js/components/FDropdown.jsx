import React from 'react';
import FDropdownMenu from './FDropdownMenu.jsx'; 
import SelectEntityActionCreator from '../actions/SelectEntityActionCreator';

export default React.createClass({
  getInitialState() {
    return {data:[]};
  },

  componentDidMount() {
  },
  
  handleClick(item) {
    // first we toggle the menu
    this.props.open = !this.props.open;
    if (this.props.open) {
      React.findDOMNode(this.refs.dropdown).className += " open";
    }
    else  {
      React.findDOMNode(this.refs.dropdown).classList.remove("open");
    }
    
    
  },
  
  getDefaultProps() {
    return {open:false};
  },
  
  render() {
    return (
      <div ref="dropdown" className="dropdown">
        <button id="single-button" onClick={this.handleClick} type="button" className="btn btn-primary" >
          Button dropdown <span className="caret"></span>
        </button>
        <FDropdownMenu ref="dropdownmenu" data={this.props.data} handleClick={this.handleClick}></FDropdownMenu>
      </div>
    );
  }
});
