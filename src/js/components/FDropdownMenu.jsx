import React from 'react';
import FMenuItem from './FMenuItem.jsx'; 

export default React.createClass({
  getInitialState() {
    return {};
  },

  componentDidMount() {
  },

  render() {
    // create an array of FMenuItems from the data
    var lis = this.props.data.map(function (item) {
      // we use this on the map to make sure it is passed into the callback function
      return (
        // we have to use bind in order to make sure the onClick is called with an item
        <FMenuItem href={item.href}  handleClick={this.props.handleClick.bind(this, item.text1)} >
          {item.text1}
        </FMenuItem>
      );
    },this);
    
    return (
        // return an unordered list of li's
        <ul className="dropdown-menu" role="menu" >
          {lis} 
        </ul>
     
    );
  }
});
