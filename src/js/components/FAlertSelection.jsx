import React from 'react';
import Alert from 'react-bootstrap/lib/Alert';
import EntityStore from '../stores/EntityStore.js';

export default React.createClass({
  getInitialState() {
    return {entity:'nothing'};
  },

  componentDidMount: function() {
    EntityStore.addChangeListener(this._onChange);
  },
  
  _onChange: function(entityVal) {
    this.setState(EntityStore.getAll());
  },

  render() {
    return (
        <Alert bsStyle="warning">
          <strong>{this.state.entity}</strong> Selected
        </Alert>
    );
  }
});
