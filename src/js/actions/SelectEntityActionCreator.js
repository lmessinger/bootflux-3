import Dispatcher from '../Dispatcher';
import Constants from '../Constants';

export default {
  selectItem(text) {
    Dispatcher.handleViewAction({
      type: Constants.ActionTypes.ENTITY_SELECTED,
      text: text
    });
  },

  clearList() {
    console.warn('clearList action not yet implemented...');
  },

  completeTask(task) {
    console.warn('completeTask action not yet implemented...');
  }
};
